package bcas.ap.inter;

public interface Shape {
	public double getArea(double w,double h);
	public double getPerimeter(double w,double h);
	public String getColor();
	public boolean isfilled();
	public int getfdges();

}
	