package bcas.ap.inter;

public class Rectangle implements Shape {

	@Override
	public double getArea(double w, double h) {
		
		return w*h;
	}

	@Override
	public double getPerimeter(double w, double h) {
	
		return 2*(w+h);
	}

	@Override
	public String getColor() {
	
		return "Red";
	}

	@Override
	public boolean isfilled() {
		// 
		return false;
	}

	@Override
	public int getfdges() {
		// 
		return 4;
	}

}
